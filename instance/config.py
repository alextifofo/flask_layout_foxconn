FLASK_APP = "app"
FLASK_ENV = "development"
SQLALCHEMY_DATABASE_URI = "postgresql://manutec:manutec@localhost/layout"
SQLALCHEMY_TRACK_MODIFICATIONS = False
SECRET_KEY = "1f912169d696ad20cf9e8e8a4fadfd8d9c54d649e25dbfd2"
