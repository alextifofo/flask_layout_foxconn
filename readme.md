<h1>Proposição de Layout</h1>

<h2>Repositório com proposição de layout para a aplicação foxconn</h2>

<p>Este repositório tem por finalidade apresentar uma estrutura modular para a aplicação a ser desenvolvida.</p>
<p>Baseada no Framework Flask, a aplicação aqui contida desenvolve algumas funcionalidades para acesso ao banco de dados e criação de modelos e migrações</p>
<p>A estrutura proposta é descrita a seguir</p>

<pre>
flask_layout_foxconn/
                    /app
                        __init__.py
                        /modules
                                __init__.py
                                /module_1/
                                         Validation.py
                                         Model.py
                                         View.py
                                         __init__.py
                                /module_2/
                                         Validation.py
                                         Model.py
                                         View.py
                                         __init__.py
                    /instance
                            config.py
                    /migrations
                            (Arquivos de migrações geradas pelo flask)
                    /venv
                            (Pasta de arquivos do ambiente virtual)
                    .gitignore
                    readme.md
                    requirements.txt
</pre>

<h2>Referências</h2>

<ul>
<li>Flask = https://flask.palletsprojects.com/en/1.1.x/</li>
<li>Flask-Migrate = https://flask-migrate.readthedocs.io/en/latest/</li>
<li>Flask-Script = https://flask-script.readthedocs.io/en/latest/ </li>
<li>Flask-SQLAlchemy = https://flask-sqlalchemy.palletsprojects.com/en/2.x/</li>
<li>WTForms = https://wtforms.readthedocs.io/en/stable/</li>
<li>zeep = https://python-zeep.readthedocs.io/en/master/</li>
</ul>