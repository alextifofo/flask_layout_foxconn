from app import app

# Import views
from .main.view import main
from .user.view import user
from .role.view import role
from .other.view import other
from .auth.view import auth
from .rest_client.view import rest_client
from .soap_client.view import soap_client

# Import Models
from .user.User import User
from .role.Role import Role

# Registrando Módulos


app.register_blueprint(user)
app.register_blueprint(role)
app.register_blueprint(main)
app.register_blueprint(other)
app.register_blueprint(rest_client)
app.register_blueprint(soap_client)
app.register_blueprint(auth)

user = User
role = Role
