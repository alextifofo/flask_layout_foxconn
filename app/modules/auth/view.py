from flask import Blueprint, request, jsonify
from app.modules.user.User import User
from app import db
from functools import wraps
import jwt

auth = Blueprint("auth", __name__, url_prefix='/auth')


@auth.route("/register", methods=["POST"])
def register():
    data = request.form
    user = User.query.filter_by(email=data['email']).first()
    if not user:
        try:
            user = User(data['name'], data['username'], data['password'], data['email'], data['role_id'])

            db.session.add(user)
            db.session.commit()

            auth_token = user.encode_auth_token(user.id)
            response_object = {
                'status': 'success',
                'message': 'Successfully registered.',
                'auth_token': auth_token.decode()
            }
            return jsonify(response_object)
        except NameError:
            response_object = {
                'status': 'fail',
                'message': 'Some error occurred. Please try again.'
            }
            return jsonify(response_object)
    else:
        response_object = {
            'status': 'fail',
            'message': 'User already exists. Please Log in.',
        }
        return jsonify(response_object)


@auth.route("/login", methods=["POST"])
def login():
    data = request.form
    try:
        user = User.query.filter_by(
            email=data.get('email')
        ).first()
        auth_token = user.encode_auth_token(user.id)
        if auth_token:
            response_object = {
                'status': 'success',
                'message': 'Successfully logged in.',
                'auth_token': auth_token.decode()
            }
            return jsonify(response_object)
    except Exception as e:
        print(e)
        response_object = {
            'status': 'fail',
            'message': 'Try again'
        }
        return jsonify(response_object)


def auth_required(f):
    @wraps(f)
    def decorator(*args, **kwargs):
        token = request.headers.get('Authorization')
        if not token:
            return jsonify({'message': 'Token is missing!'})
        try:
            data = jwt.decode(token, 'foxconn_secret_key')
            current_user = User.query.filter_by(id=data.get('sub')).first()
        except jwt.ExpiredSignatureError:
            return jsonify({'message': 'Token is invalid or expired!'})

        return f(current_user, *args, **kwargs)

    return decorator
