from flask import Blueprint, request, jsonify
from .Role import Role
from app import db
from .ValidationRole import ValidationRole

role = Blueprint("role", __name__, url_prefix="/role")


@role.route('', methods=['POST'])
def create():
    valid = ValidationRole(request.form)

    if request.method == 'POST' and valid.validate():
        data = request.form
        me = Role(data['name'])
        db.session.add(me)
        db.session.commit()
        return "Usuário inserido com susesso"

    if valid.name.errors:
        return "%s" % format(valid.name.errors)


@role.route("")
def show():
    try:
        roles = Role.query.limit(10).all()
        return jsonify(json_list=[role.serialize for role in roles])
    except:
        return jsonify(error="Falha ao consultar a base de dados.")


@role.route("<name>")
def profile_name(name):
    try:
        role = Role.query.filter_by(name=name).first()
        return jsonify(json_list=role.serialize)
    except:
        return jsonify(error="Falha ao consultar a base de dados.")


@role.route("<int:id>")
def profile_id(id):
    try:
        role = Role.query.get(id)
        return jsonify(json_list=role.serialize)
    except NameError:
        return jsonify(error="Role not found.")


@role.route('<int:roleid>', methods=['PUT'])
def update(roleid):
    try:
        data = request.form
        role = Role.query.get(roleid)
        role.name = data['name']
        db.session.commit()
        return jsonify(sucesso="Papel atualizado com sucesso!")
        # return "Papel atualizado com sucesso!"
    except NameError:
        return "Erro ao atualizar este papel"
