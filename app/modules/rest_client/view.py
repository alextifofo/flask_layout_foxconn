import requests
import json
from flask import Blueprint, jsonify

rest_client = Blueprint('rest_client', __name__, url_prefix="/rest")


@rest_client.route("/<string:busca>")
def index(busca):
    response = requests.get(
        "https://api.github.com/search/repositories",
        params={
            'q': busca
        }
    )
    if response.status_code != 200:
        return response.status_code

    return json.loads(response.content)["items"][0]
