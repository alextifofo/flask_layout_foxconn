import zeep
import json
from flask import Blueprint, jsonify

# Para obter as operações disponíveis para um serviço, entrar com o seguinte comando
# python -mzeep http://www.soapclient.com/xml/soapresponder.wsdl

wsdl = 'http://www.soapclient.com/xml/soapresponder.wsdl'
soap_client = Blueprint('soap_client', __name__, url_prefix='/soap')


@soap_client.route("/<string:param1>/<string:param2>")
def index(param1, param2):
    re = [param1, param2]
    client = zeep.Client(wsdl=wsdl)
    result = client.service.Method1(param1, param2)
    return jsonify(result=result, params=re)

