from flask import Blueprint, jsonify

main = Blueprint("main", __name__, url_prefix="/")


@main.route("")
def index():
    return jsonify(main="Módulo principal")

