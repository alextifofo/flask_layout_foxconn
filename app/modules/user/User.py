import jwt
import datetime
from app import app, db
from app.modules.role.Role import Role


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(180), nullable=False)
    username = db.Column(db.String(80), unique=True, nullable=False)
    password = db.Column(db.String(80), unique=False)
    email = db.Column(db.String(120), unique=True, nullable=False)
    role_id = db.Column(db.Integer, db.ForeignKey('role.id'),
                        nullable=False)
    role = db.relationship('Role',
                           backref=db.backref('roles', lazy=True))

    def __init__(self, name, username, password, email, role_id):
        self.name = name
        self.username = username
        self.password = password
        self.email = email
        self.role_id = role_id

    def __repr__(self):
        return '<User %r>' % self.username

    @staticmethod
    def encode_auth_token(user_id):
        payload = {
            'exp': datetime.datetime.utcnow() + datetime.timedelta(seconds=120),
            'iat': datetime.datetime.utcnow(),
            'sub': user_id
        }
        try:
            return jwt.encode(payload, 'foxconn_secret_key', algorithm='HS256')
        except Exception as e:
            return e

    @staticmethod
    def decode_auth_token(auth_token):
        """
        Decodes the auth token
        :param auth_token:
        :return: integer|string
        """
        try:
            payload = jwt.decode(auth_token, app.config.get('SECRET_KEY'))
            is_blacklisted_token = BlacklistToken.check_blacklist(auth_token)
            if is_blacklisted_token:
                return 'Token blacklisted. Please log in again.'
            else:
                return payload['sub']
        except jwt.ExpiredSignatureError:
            return 'Signature expired. Please log in again.'
        except jwt.InvalidTokenError:
            return 'Invalid token. Please log in again.'

    @property
    def serialize_role(self):
        role = Role.query.get(self.role_id)
        return role.serialize

    @property
    def serialize(self):
        """Return object data in easily serializable format"""
        return {
            'id': self.id,
            'name': self.name,
            'username': self.username,
            'password': self.password,
            'email': self.email,
            'role': self.serialize_role
        }