from app import db
from flask import Blueprint, jsonify, request
from .User import User
from ..auth import view
from .ValidationUser import ValidationUser

user = Blueprint("user", __name__, url_prefix="/user")


@user.route("index")
@view.auth_required
def index(current_user):
    return f"Modulo do usuário {current_user.username}"


@user.route('', methods=['POST'])
def create():
    valid = ValidationUser(request.form)

    if request.method == 'POST' and valid.validate():
        data = request.form
        me = User(data['name'], data['username'], data['password'], data['email'], data['role_id'])
        db.session.add(me)
        db.session.commit()
        return "Usuário inserido com susesso"

    if valid.username.errors:
        return "%s" % format(valid.username.errors)
    if valid.email.errors:
        return "%s" % format(valid.email.errors)
    if valid.name.errors:
        return "%s" % format(valid.name.errors)
    if valid.password.errors:
        return "%s" % format(valid.password.errors)


@user.route('/<username>')
def show_user_profile(username):
    # show the user profile for that user
    user = User.query.filter_by(username=username).first()
    if user:
        return jsonify(user=user.serialize)
    return jsonify(erro="User not found.")

@user.route('/<int:id>')
def show_user_profile_id(id):
    # show the user profile for that user
    user = User.query.get(id)
    if user:
        return jsonify(user=user.serialize)
    return jsonify(erro="User not found.")


@user.route('/all', methods=['GET'])
def show_users():
    try:
        result = User.query.limit(100).all()
        return jsonify(json_list=[user.serialize for user in result])
    except:
        return jsonify(erro="Users not found.")


@user.route('<int:user_id>', methods=['PUT'])
def users_update(user_id):
    campos = dict(request.form)
    print(campos)
    data = request.form
    user = User.query.get(user_id)
    if user:
        try:
            if 'name' in data:
                user.name = data['name']
            if 'email' in data:
                user.email = data['email']
            if 'password' in data:
                user.password = data['password']
            if 'username' in data:
                user.username = data['username']
            db.session.commit()
            return jsonify(sucesso="Sucesso")
        except NameError:
            return jsonify(sucesso="Fracasso")
    else:
        return jsonify(sucesso="Not found")


@user.route('', methods=["DELETE"])
def delete():
    id = request.form.get('id')
    user = User.query.get(id)

    if user != None:
        try:
            db.session.delete(user)
            db.session.commit()
            return jsonify("Usuário Removido com sucesso!")
        except NameError:
            return jsonify(error="Database error.")
    else:
        return jsonify("User not found.")
