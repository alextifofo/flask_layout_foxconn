from wtforms import Form, validators, StringField


class ValidationUser(Form):
    username = StringField('username',
                           [
                               validators.Length(min=4, message="tamanho mínimo 4 caracteres"),
                               validators.Length(max=40, message="tamanho máximo 40 caracteres"),
                               validators.InputRequired(message="parametro username obrigatório")
                           ]
                           )
    name = StringField('name', [
        validators.InputRequired(message="parametro name obrigatório")
    ])

    password = StringField('password', [
        validators.InputRequired(message="parametro password obrigatório")
    ])

    email = StringField(
        'email',
        [
            validators.InputRequired(message="parametro email obrigatório"),
            validators.Length(min=6, max=35),
            validators.Email(message='Não é um email válido!'
                             )
        ])
