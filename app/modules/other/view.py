from flask import Blueprint

other = Blueprint("other", __name__, url_prefix="/other")


@other.route("")
def index():
    return "Modulo n..."
